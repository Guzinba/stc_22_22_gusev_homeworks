public abstract class AbstractNumbersPrintTask implements Task {
    public int from;
    public int to;

    public AbstractNumbersPrintTask(int from, int to){
        this.from = from;
        this.to = to;
    }
    public abstract void complete();

    public int getFrom(){
        return from;
    }
    public int getTo(){
        return to;
    }
}

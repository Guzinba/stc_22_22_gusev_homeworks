public class Main {
    public static void completeAllTasks(Task[]tasks){
        for(int i = 0; i < tasks.length; i++){
            tasks[i].complete();
        }
    }
    public static void main(String[] args) {
        OddNumbersPrintTask oddNumbersPrintTask = new OddNumbersPrintTask(2, 7);
        EvenNumbersPrintTask evenNumbersPrintTask = new EvenNumbersPrintTask(6, 8);
        Task[] tasks = {evenNumbersPrintTask, oddNumbersPrintTask};

        completeAllTasks(tasks);
    }

}
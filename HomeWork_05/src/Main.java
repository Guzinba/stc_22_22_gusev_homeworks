public class Main {
    public static void main(String[] args) {
        ATM atm = new ATM(100, 50, 200, 5);

        System.out.println(atm.getMaxSum());
        System.out.println(atm.give(30));
        System.out.println(atm.take(27));
        System.out.println(atm.getOper());
    }
}
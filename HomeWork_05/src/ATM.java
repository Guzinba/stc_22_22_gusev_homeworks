public class ATM {
    private double sum;
    private double giveSum;
    private double maxSum;
    private double oper;

    public ATM(double sum, double giveSum, double maxSum, double oper){
        this.sum = sum;
        this.giveSum = giveSum;
        this.maxSum = maxSum;
        this.oper = oper;

    }
    public int give(int givings){
        int result = 0;
        if(givings <= maxSum & givings <= giveSum){
            result = givings;
            oper++;
        }
    return result;
    }

    public int take(int takings){
        int result1 = 0;
        if ((takings + sum ) <= maxSum) {
            result1 = takings;
            oper++;
        }
    return result1;
    }
    public double getSum(){
        return sum;
    }
    public double getMaxSum(){
        return maxSum;
    }
    public double getGiveSum(){
        return giveSum;
    }
    public double getOper(){
        return oper;
    }
}

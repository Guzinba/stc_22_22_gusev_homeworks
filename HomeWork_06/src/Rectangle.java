public class Rectangle extends Square {
    int width;
    public Rectangle(int x, int y, int len, int width){
        super(len, x, y);
        this.width = width;
    }
    public int getWidth(){
        return width;
    }
    public int square(){
        return len * width;

    }
    public int perimeter(){
        return 2*len + 2*width;
    }
}

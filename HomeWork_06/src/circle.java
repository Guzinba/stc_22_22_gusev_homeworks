public class circle extends Figure{
    int r1;
    public circle(int x, int y, int r1){
        super(x, y);
        this.r1 = r1;
    }
    public int getR1(){
        return r1;
    }
    public double square(){
        return Math.PI * r1*r1;
    }
    public double perimeter(){
        return Math.PI * 2 * r1;
    }
}

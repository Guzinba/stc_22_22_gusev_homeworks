public class Elips extends circle {

    private int r2;
    public Elips(int r1, int r2, int x, int y){
        super(x, y, r1);
        this.r2 = r2;
    }
    public int getR2(){
        return r2;
    }
    public double square(){
        return Math.PI * r1 * r2;
    }
    public double perimeter(){
        return Math.PI * 2 * Math.sqrt((double)(r1*r1 + r2*r2) / 2);
    }
}

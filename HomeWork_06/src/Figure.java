public class Figure {
    private int x;
    private int y;

    public Figure(int x, int y){
        this.x = x;
        this.y = y;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int[] move(int toX, int toY){
        x = toX;
        y = toY;
        return new int[]{x, y};
    }
}

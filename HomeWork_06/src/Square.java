public class Square extends Figure {
    int len;
    public Square(int len, int x, int y){
        super(x, y);
        this.len = len;
    }
    public int getLen(){
        return len;
    }
    public int square(){
        return len * len;

    }
    public int perimeter(){
        return 4*len;
    }
}
